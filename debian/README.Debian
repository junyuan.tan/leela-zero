Installing the right OpenCL ICD loader
======================================

There are several packages that provide opencl-icd, you need to pick the one
that matches your graphics driver (`glxinfo | grep -i "vendor\|rendering"`).
To check that you have the right one, run::

  clinfo -l

You should see more than one "Device" listed. If you only see "Platform" but
no "Device", or if you see nothing, then you haven't installed the right ones.


Before you start
================

Before running leelaz "properly" for the first time (i.e. to play a game) you
probably want to first run::

  mkdir -p ~/.cache/leela-zero ~/.local/share/leela-zero
  ln -sf ../../../.cache/leela-zero/best-network ~/.local/share/leela-zero/
  cd ~/.cache/leela-zero && wget https://zero.sjeng.org/best-network -O best-network

This downloads the currently-best network. If you want, set up a cron job to
update it periodically. To improve performance you might also want to run:

  leelaz --full-tuner --tune-only

This can take up to an hour and requires a weights file, e.g. the one that you
downloaded in the previous step.

The source data of the best-network is in the public domain [1] and the steps
to generate it is described in the README. However currently this requires
software not in the "main" section of Debian, so if this is an issue for you
you can instead use a "dummy" weights file as follows:

  mkdir -p ~/.cache/leela-zero
  cd ~/.cache/leela-zero
  python3 -c 'print("1\n" + "".join(("0 " * n).strip() + "\n" for n in [31104,192,192,192] + [331776,192,192,192] * 30 + [384,2,2,2,261364,362,192,1,1,1,92416,256,256,1]), end="")' > dummy-network

Then you can pass "--weights ~/.cache/leela-zero/dummy-network" in order to use
use this file. Or instead just redirect the above to "best-network" if you are
happy with the false naming.

[1] https://github.com/gcp/leela-zero/issues/167


Running with other programs
===========================

You can add Leela Zero as an AI Go player for game GUIs that support GTP,
simply configure a new AI program to run::

  leelaz --gtp -q

Currently (2018-07-14) it works well with Quarry but does not work with qGo, see
https://github.com/pzorin/qgo/issues/54.

When playing an actual game you most likely want to set a time limit, otherwise
leelaz may continue thinking for a very very long time. The --noponder flag is
also useful if you don't want your GPU continuously running.
